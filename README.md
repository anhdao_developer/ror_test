# Task description #
## Ruby on Rails sample programming task ##
Implement the calculator controller at app/controllers/calculator_controller.rb. Calculator controller should perform following mathematical operations and set a correct `@result` variable

Add - if `@operation` is equal to add

Subtract - if `@operation` is equal to divide

Multiply - if `@operation` is equal to multiply

Divide - if `@operation` is equal to divide

If it is impossible to perform requested operation, because of invalid input data, `@result` should be set to 'ERROR'.

You can check unit tests in test/controllers/calculator_controller_test.rb for detailed requirements.